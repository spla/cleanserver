import getpass
import fileinput,re
import os
import sys
import pdb

def create_config():

    if not os.path.exists('config'):
        os.makedirs('config')
    if not os.path.exists(config_filepath):
        print(config_filepath + " created!")
        with open('config/config.txt', 'w'): pass

def write_config():

    with open(config_filepath, 'a') as the_file:

        the_file.write('mastodon_hostname: \n')
        print(f"adding parameter 'mastodon_hostname' to {config_filepath}")
        the_file.write('mastodon_full_path: \n')
        print(f"adding parameter 'mastodon_full_path' to {config_filepath}")
        the_file.write('purge_days: \n')
        print(f"adding parameter 'purge_days' to {config_filepath}")

def read_config_line():

    with open(config_filepath) as fp:

        line = fp.readline()
        modify_file(config_filepath, "mastodon_hostname: ", value=hostname)
        modify_file(config_filepath, "mastodon_full_path: ", value=full_path)
        modify_file(config_filepath, "purge_days: ", value=purge_days)

def modify_file(file_name,pattern,value=""):

    fh=fileinput.input(file_name,inplace=True)

    for line in fh:

        replacement=pattern + value
        line=re.sub(pattern,replacement,line)
        sys.stdout.write(line)

    fh.close()

def get_parameter( parameter, config_filepath ):

    # Check if secrets file exists
    if not os.path.isfile(config_filepath):

        print(f'File {config_filepath} not found, creating it.')
        create_config()

    # Find parameter in file
    with open( config_filepath ) as f:

        for line in f:

            if line.startswith( parameter ):

                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(f'{config_filepath} Missing parameter {parameter}')

    write_config()

    global hostname, full_path, purge_days

    hostname = input("Enter Mastodon hostname: ")
    full_path = input("Enter Mastodon's full path dir: ")
    purge_days = int(input("Enter days to trigger server purge (default 7): ") or "7")
    purge_days = str(purge_days)
    
    read_config_line()

    print("cleanserver setup done!")
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    mastodon_full_path = get_parameter("mastodon_full_path", config_filepath)
    purge_days = get_parameter("purge_days", config_filepath)
