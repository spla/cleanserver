# Cleanserver
Cleanserver get & check if all of the peers of your managed Mastodon server are or are not alive, count their 'downs' and, after seven days (or downs), purge them to remove their data from your Mastodon server's database.  

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon server  
-   Mastodon server admin account

### Usage:

1. Clone this repo: `git clone https://gitlab.com/spla/cleanserver.git <target dir>`  

2. cd into your `<target dir>` and create the Python Virtual Environment: `python3.x -m venv .`  
 
3. Activate the Python Virtual Environment: `source bin/activate`  

4. Run `pip install -r requirements.txt` to install needed libraries.

5. Run `python db-setup.py` to setup and create new Postgresql database and needed tables.

6. Run `python setup.py` to setup your Mastodon hostname and install dir full path.

7. Use your favourite scheduling method to set `python cleanserver.py` to run once a day.  
